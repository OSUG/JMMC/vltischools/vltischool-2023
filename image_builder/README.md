**To create cloud machine images, we employed the Ansible local provisioner in conjunction with Packer, which functions effectively for both branches of ELKH Cloud.**

## The ELKH Cloud, available at https://science-cloud.hu/en, consists of two branches
- WDC Wigner Data Center
- SZTAKI (Institute for Computer Science and Control) 

## The OpenStack cloud implementation varies between these branches
- The WDC branch, access is provided via VPN (OpenVPN) with only one active VPN connection per project allowed to the cloud. This branch employs a unique network solution for security. One of the consequences is the absence of floating IPs.
- The SZTAKI branch permits eduID identifier login and allows the use of one floating IP per project, which is a public IP.