#!/bin/bash -eux

# Install Ansible
sudo apt-get -y update 
sudo apt-get install -y dialog debconf-utils
sudo echo 'debconf debconf/frontend select Noninteractive' | sudo debconf-set-selections
sudo apt-get -y install ansible


