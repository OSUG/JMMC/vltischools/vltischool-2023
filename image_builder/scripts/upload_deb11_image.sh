#!/bin/bash

# Get the current date in the format YYYY-MM-DD
current_date=$(date +%Y-%m-%d)

# Define the image name
image_name="debian-11-x86_64_${current_date}"

# Check if the image with the same name already exists
existing_image=$(openstack --insecure image list --private -c Name -f value | grep "${image_name}")

# If the image does not exist, download and upload it
if [ -z "${existing_image}" ]; then
  # Download the image and checksum file
  wget https://cloud.debian.org/cdimage/cloud/bullseye/daily/latest/debian-11-genericcloud-amd64-daily.qcow2
  wget https://cloud.debian.org/cdimage/cloud/bullseye/daily/latest/SHA512SUMS

  # Extract the appropriate line from the SHA512SUMS file
  grep debian-11-genericcloud-amd64-daily.qcow2 SHA512SUMS > SHA512SUMS_filtered

  # Verify the checksum of the downloaded image
  sha512sum -c SHA512SUMS_filtered
  if [ $? -eq 0 ]; then
    # Create the image using OpenStack CLI
    openstack --insecure image create \
      --container-format bare \
      --disk-format qcow2 \
      --property hw_disk_bus=scsi \
      --property hw_scsi_model=virtio-scsi \
      --property os_type=linux \
      --property os_distro=debian \
      --property os_admin_user=debian \
      --private \
      --file debian-11-genericcloud-amd64-daily.qcow2 "${image_name}"

    # Delete the downloaded image and checksum files
    rm -f debian-11-genericcloud-amd64-daily.qcow2 SHA512SUMS SHA512SUMS_filtered
  else
    echo "Checksum verification failed. Aborting."
    exit 1
  fi
else
  echo "Image '${image_name}' already exists, skipping download and upload."
fi

