#!/bin/bash -eux

# Uninstall Ansible
sudo apt-get -y remove --purge ansible


# Apt cleanup.
sudo apt-get -y autoremove
sudo apt-get -y update
sudo echo 'debconf debconf/frontend select Dialog' | sudo debconf-set-selections
