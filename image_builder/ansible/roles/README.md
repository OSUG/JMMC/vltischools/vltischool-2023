**Note: The Ansible roles are listed in the order of installation. The installation of the roles builds on each other. Only one of the xfce or mate desktop environments will be installed.**


## Configure time

- In the case of the ELKH cloud SZTAKI branch, setting up Hungarian NTP servers.
- in the case of the WDC branch, setting up time.kfki.hu, which is allowed by the firewalls.

## Generate locals

- Avoid many warnings in the Ansible messages

## User setup

- Create vlti sudo user

## install_mate

- mate-desktop

## install_xfce

- xcfce4
- task-xfce-desktop

## install_x2go

- adding x2go's repository
- x2goserver
- x2goserver-xsession
- x2goserver-desktopsharing
- lightdm-remote-session-x2go

## disable_screensaver

- Disable screensaver functionality for every user in the case of the Mate desktop environment.

- In the case of cloud machines, it doesn't make sense to enable the screensaver and it disables key-based authentication only.


## install_vscode

- Add visual Studio Code repository
- Install Visual Studio Code

## install_matebindings

- x2gomatebindings


## install_docker

- Add docker-ce repository
- docker-ce
- docker-ce-cli
- containerd.io
- docker-buildx-plugin
- docker-compose-plugin

## install_sctools

- virtualenv
- python3-virtualenv
- python3-venv
- python3-setuptools
- htop
- mc
- ftools-fv
- tk
- icedtea-netx
- xemacs21
- emacs
- vim-gui-common
- gedit
- meld
- saods9


## install_venv 

- Create a Python environment named Venv, in which only the root user can install packages:

  - a2p2
  - jupyterlab 
  - spyder
  - spyder-kernels
  - scipy
  - astropy
  - astroquery
  - matplotlib
  - jupyterlab
  - ipympl
  - pandas

## School Profile

- Activate the venv python environment

- aliases
  - ll (ls -alF)
  - ls (ls --color=auto)
  - lt (ls -ltr)
  - o  (xdg-open)


## jmmc_tools

- aliases
  - AppLauncher
  - applauncher
  - Aspro2
  - aspro2
  - LITpro
  - litpro
  - SearchCal
  - searchcal
  - OIFitsExplorer
  - oifitsexplorer
  - OImaging
  - oimaging

## install_pmoired

 - Install PMOIRED in the venv environment in the /opt directory. 
 - Tutorials can be copied from here /otp/PMOIRED/ to the users' own directories



