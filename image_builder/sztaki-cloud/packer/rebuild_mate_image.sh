#!/bin/bash


# Set the name of the image you want to delete
image_name="vlti2023_mate_base"

# Get the image ID
image_id=$(openstack image list --name "$image_name" -f json | jq -r '.[] | select(.Name=="vlti2023_mate_base") | .ID')

# If the image exists, delete it
if [ -n "$image_id" ]; then
  echo "Deleting existing image with ID: $image_id"
  openstack image delete "$image_id"
else
  echo "No image found with the name: $image_name"
fi

# Start the Packer build
packer build deb11_fmate_x2go_docker.pkr.hcl
