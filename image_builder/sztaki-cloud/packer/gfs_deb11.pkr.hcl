variables {
  image_name         = "vlti_gfs_2023-06-18"
  source_image_name  = "debian-11-x86_64_2023-06-18"
  networks           = ["78952151-b45d-4668-8204-98ddd1da7354"] # openstack --insecure network list | grep default | awk '{print $2}'
  security_groups    = ["default"]
  floating_ip_network ="229d5e38-37db-44fd-af39-c1da0b651706" # openstack --insecure network list | grep ext-net | awk '{print $2}'
}

source "openstack" "debian" {
  image_name         = var.image_name
  source_image_name  = var.source_image_name
  insecure           = false
  use_floating_ip    = true
  ssh_username       = "debian"
  use_blockstorage_volume = true
  volume_size        = 4
  image_disk_format  = "raw"
  flavor             = "r2.large"
  networks           = var.networks
  floating_ip_network  = var.floating_ip_network
  security_groups    = var.security_groups
}


build {
  sources = ["source.openstack.debian"]

  provisioner "shell" {
    script = "../../scripts/ansible.sh"
  }

  provisioner "ansible-local" {
    playbook_file = "../../ansible/glusterfs_server_sztaki.yml"
    role_paths = [
      "../../ansible/roles/generate_locale",
      "../../ansible/roles/configure_time",
      "../../ansible/roles/install_glusterfs",
      "../../ansible/roles/install_mdadm"
    ]
  }

  provisioner "shell" {
    script = "../../scripts/cleanup.sh"
  }
}
