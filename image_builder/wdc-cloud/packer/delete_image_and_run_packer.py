#!/usr/bin/env python3

import openstack

# Set the name of the image you want to delete
image_name = "vlti2023_mate_base"
project_id = "3d10e0fc381b4cfeb93565654c8f7752"

# Create a connection to the OpenStack cloud
conn = openstack.connect(cloud='openstack', verify=False)

# Find all images
all_images = list(conn.image.images())

# Loop through images and delete the one with matching name and owner
image_found = False
for image in all_images:
    if image.name == image_name and image.get("owner") == project_id:
        print(f"Deleting existing image with ID: {image.id}")
        conn.image.delete_image(image.id)
        image_found = True
        break

if not image_found:
    print(f"No image found with the name: {image_name} and project_id: {project_id}")

# Start the Packer build
packer_command = "packer build deb11_fmate_x2go_docker.pkr.hcl"
import subprocess
subprocess.run(packer_command.split())
