variables {
  image_name         = "vlti_gfs_2023-06-12"
  source_image_name  = "debian-11-x86_64_2023-06-12"
  networks           = "01aeef63-8d6e-4623-8bae-6b6c4ba13431" # openstack --insecure network list | grep vlti | awk '{print $2}'
  security_groups    = "default"
  floating_ip_network = ""
  vault_password_file = ""
}


source "openstack" "debian" {
  image_name         = var.image_name
  source_image_name  = var.source_image_name
  insecure           = true
  use_floating_ip    = false
  ssh_username       = "debian"
  use_blockstorage_volume = true
  volume_size        = 4
  image_disk_format  = "raw"
  flavor             = "r2.large"
  networks           = [var.networks]
  security_groups    = [var.security_groups]
}


build {
  sources = ["source.openstack.debian"]

  provisioner "shell" {
    script = "../../scripts/ansible.sh"
  }

  provisioner "ansible-local" {
    playbook_file = "../../ansible/glusterfs_server_wdc.yml"
    role_paths = [
      "../../ansible/roles/generate_locale",
      "../../ansible/roles/configure_time_wdc",
      "../../ansible/roles/install_glusterfs",
      "../../ansible/roles/install_mdadm"
    ]
  }

  provisioner "shell" {
    script = "../../scripts/cleanup.sh"
  }
}
