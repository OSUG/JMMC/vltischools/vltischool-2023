variables {
  image_name         = "vlti2023_mate_base_2023-06-12"
  source_image_name  = "debian-11-x86_64_2023-06-12"
  networks           = "01aeef63-8d6e-4623-8bae-6b6c4ba13431" # openstack --insecure network list | grep vlti | awk '{print $2}'

  security_groups    = "default"
  floating_ip_network = ""
  vault_password_file = ""
}

source "openstack" "debian" {
  image_name         = var.image_name
  source_image_name  = var.source_image_name
  insecure           = true
  use_floating_ip    = false
  ssh_username       = "debian"
  use_blockstorage_volume = true
  volume_size        = 16
  image_disk_format  = "raw"
  flavor             = "m2.2xlarge"
  networks           = [var.networks]
  security_groups    = [var.security_groups]
}

build {
  sources = ["source.openstack.debian"]

  provisioner "shell" {
    script = "../../scripts/ansible.sh"
  }

  provisioner "ansible-local" {
    playbook_file = "../../ansible/fmate-x2go-docker_wdc.yml"
    role_paths = [
      "../../ansible/roles/generate_locale",
      "../../ansible/roles/configure_time_wdc",
      "../../ansible/roles/install_glusterfs_client",
      "../../ansible/roles/install_mate",
      "../../ansible/roles/install_x2go",
      "../../ansible/roles/install_matebindings",
      "../../ansible/roles/install_docker",
      "../../ansible/roles/install_sctools",
      "../../ansible/roles/install_venv",
      "../../ansible/roles/jmmc_tools",
      "../../ansible/roles/install_pmoired",
      "../../ansible/roles/user_setup",
      "../../ansible/roles/school_profile",
      "../../ansible/roles/disable_screensaver",
      "../../ansible/roles/disable_mate_compositor",
      "../../ansible/roles/install_vscode",
      "../../ansible/roles/docker_deploy_oiservices",
      "../../ansible/roles/docker_deploy_litpro",
      "../../ansible/roles/run_profilescripts_in_mate"
    ]
 
  }

  provisioner "shell" {
    script = "../../scripts/cleanup.sh"
  }
}
