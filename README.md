# VlTI SCHOOL 2023 

## Tutorials on ELKH cloud's instances



## Preparations 

Preparations for the 11th VLTI School of the European Interferometry Initiative, Budapest, June 12-17, 2023 on the ELKH Cloud

- https://www.eso.org/sci/publications/announcements/sciann17503.html
- https://vltischool2023.konkoly.hu/

## Links to gitlab repositories

- https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/vltischools/vltischool-2023
- https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/vltischools/vltischool-2021

## Links to the 10th VLTI School of Interferometry in 2021

- https://vltischool2021.sciencesconf.org/
- http://www.jmmc.fr/schools/vltischool2021/
- https://pod.univ-cotedazur.fr/vlti-school-2021/

## 2023 VLTI School Schedule

- [Schedule](./schedule.png)

