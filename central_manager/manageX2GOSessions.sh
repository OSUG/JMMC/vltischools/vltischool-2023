#!/bin/bash

set -e
# get passwords from files
#X2PASS=$(cat vltis.password) 
X2PASS_VLTI=$(cat vlti.password) 

echo "# SSH or Create x2go or Join group sessions on sztacki"
for i in $( seq 120 134 )
do
  NB=$(printf "%02d" $i)
  X2HOST=vlti$NB.science-cloud.hu 
  X2USER=vlti 
  PYHOCA=" pyhoca-cli --server $X2HOST --user $X2USER --kbd-layout us --kbd-type pc105/us --password $X2PASS_VLTI --force-password"
  echo 
  echo  "ssh $X2USER@$X2HOST"
  echo " $PYHOCA -N -c MATE --try-resume -g 1680x1050 & "
  echo " $PYHOCA --share-mode 1 --sound none --share-desktop $X2USER@:51"
  echo " $PYHOCA -L"
done

echo
echo
echo

for i in $( seq 0 14 )
do
  NB=$(printf "%02d" $i)
  X2HOST="193.224.32.1"
  PYHOCA=" pyhoca-cli --server $X2HOST --user $X2USER --kbd-layout us --kbd-type pc105/us --port 200$NB --password $X2PASS_VLTI --force-password"

  echo 
  echo  "ssh $X2USER@$X2HOST "
  echo "$PYHOCA -N -c MATE --try-resume -g 1680x1050i &" 
  echo "$PYHOCA --share-mode 1 --sound none -share-desktop $X2USER@:51"
  echo "$PYHOCA -L"
done


echo 
