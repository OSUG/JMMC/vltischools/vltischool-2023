# Define variables
variable "gfs_network" {
  type = object({
    name = string
    network_id = string
    network_subnet_range = string
    network_subnet_id = string
  })
  default=({
    name = "default"
    network_id = "78952151-b45d-4668-8204-98ddd1da7354" # openstack network list | grep default | awk '{print $2}'
    network_subnet_range = "192.168.0.0/24" # subnet show 908bbe04-99f2-4a71-be21-f361886dd744 -f value -c cidr
    network_subnet_id = "908bbe04-99f2-4a71-be21-f361886dd744"  # openstack network list | grep default | awk '{print $6}'
  })
}

variable "gfs_servers_config" {
  description = "Configuration for the GlusterFS servers."
  type = object({
    flavor_name = string
    key_pair = string
    volume_size = number
    raid_volume_size = number
    volume_type = string
    security_groups = list(string)
    user_data_file = string
    image_name = string
    image_id = string
    source_type = string
    raid_source_type = string
    destination_type = string
  })
  default = {
    flavor_name = "r2.large"
    key_pair = "vlti"
    volume_size = 20
    raid_volume_size = 500
    volume_type ="SSD"
    security_groups = ["default"]
    user_data_file = "./cloud-init/gfs_cloud-init.yaml"
    image_name = "vlti_gfs_2023-06-18"
    image_id = "5d9140da-c147-483f-bded-89003c224a79" 
    source_type = "image"
    raid_source_type = "blank"
    destination_type ="volume"
  }
}
