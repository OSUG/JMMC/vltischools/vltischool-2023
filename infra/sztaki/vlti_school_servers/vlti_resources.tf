# GlusterFs server group
resource "openstack_compute_servergroup_v2" "glusterfs_group" {
  name     = "vlti"
  policies = ["soft-anti-affinity"]
}

# GlusterFs servers
resource "openstack_compute_instance_v2" "vlti_server" {
  for_each = { for instance in local.vlti_servers : instance.name => instance }

  name            = each.value.name
  flavor_name     = each.value.flavor_name
  key_pair        = each.value.key_pair
  security_groups = each.value.security_groups
  user_data       = each.value.user_data

  scheduler_hints {
    group = openstack_compute_servergroup_v2.glusterfs_group.id
  }

  network {
    port = openstack_networking_port_v2.vlti_port[each.key].id
  }

  dynamic "block_device" {
    for_each = local.block_devices
    content {
      source_type           = block_device.value.source_type
      volume_size           = block_device.value.volume_size
      volume_type           = block_device.value.volume_type
      boot_index            = block_device.value.boot_index
      destination_type      = block_device.value.destination_type
      delete_on_termination = block_device.value.delete_on_termination
      uuid                  = lookup(block_device.value, "uuid", null)
    }
  }
}

resource "openstack_networking_port_v2" "vlti_port" {
  for_each = { for instance in local.vlti_servers : instance.name => instance }

  name           = "${each.value.name}-fixed-ip-port"
  network_id     = var.vlti_network.network_id
  admin_state_up = "true"

  fixed_ip {
    subnet_id  = var.vlti_network.network_subnet_id
    ip_address = each.value.ip_address
  }
}

data "openstack_networking_floatingip_v2" "vlti_fip" {
  for_each = var.floating_ips
  address  = each.value
}
resource "openstack_compute_floatingip_associate_v2" "vlti_fip_assoc" {
  for_each = data.openstack_networking_floatingip_v2.vlti_fip

  floating_ip = each.value.address
  instance_id = openstack_compute_instance_v2.vlti_server[keys(var.floating_ips)[index(values(var.floating_ips), each.value.address)]].id
}

