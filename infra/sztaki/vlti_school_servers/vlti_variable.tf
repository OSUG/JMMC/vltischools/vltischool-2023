# Define variables
variable "vlti_network" {
  type = object({
    name = string
    network_id = string
    network_subnet_range = string
    network_subnet_id = string
  })
  default=({
    name = "default"
    network_id = "78952151-b45d-4668-8204-98ddd1da7354" # openstack network list | grep default | awk '{print $2}'
    network_subnet_range = "192.168.0.0/24" # subnet show 908bbe04-99f2-4a71-be21-f361886dd744 -f value -c cidr
    network_subnet_id = "908bbe04-99f2-4a71-be21-f361886dd744"  # openstack network list | grep default | awk '{print $6}'
  })
}

variable "vlti_servers_config" {
  description = "Configuration for the VLTI SERVERS servers."
  type = object({
    flavor_name = string
    key_pair = string
    volume_size = number
    volume_type = string
    security_groups = list(string)
    user_data_file = string
    image_name = string
    image_id = string
    source_type = string
    destination_type = string
  })
  default = {
    flavor_name = "m2.xlarge"
    key_pair = "vlti"
    volume_size = 160
    volume_type ="SSD"
    security_groups = ["default"]
    user_data_file = "./cloud-init/vlti_cloud-init.yaml"
    image_name = "vlti_mate_base_2023-06-18"
    image_id = "4ddab7f3-6f17-4e19-ab83-f644b92807da" 
    source_type = "image"
    destination_type ="volume"
  }
}

variable "floating_ips" {
  type = map(string)
  default = {
    "vlti120" = "193.225.250.38",
    "vlti121" = "193.225.250.64",
    "vlti122" = "193.225.250.74",
    "vlti123" = "193.225.250.122",
    "vlti124" = "193.225.250.147",
    "vlti125" = "193.225.250.204",
    "vlti126" = "193.225.250.212",
    "vlti127" = "193.225.250.223",
    "vlti128" = "193.225.251.48",
    "vlti129" = "193.225.251.82",
    "vlti130" = "193.225.251.88",
    "vlti131" = "193.225.251.149",
    "vlti132" = "193.225.251.156",
    "vlti133" = "193.225.251.185",
    "vlti134" = "193.225.251.212",
    "vlti135" = "193.225.250.3",
    "vlti136" = "193.225.250.7",
    "vlti137" = "193.225.250.15",
    "vlti138" = "193.225.250.45",
    "vlti139" = "193.225.250.92",
    "vlti140" = "193.225.250.116",
    "vlti141" = "193.225.250.152",
    "vlti142" = "193.225.250.153",
    "vlti143" = "193.225.250.248",
    "vlti144" = "193.225.251.90",
    "vlti145" = "193.225.251.133",
    "vlti146" = "193.225.251.138",
    "vlti147" = "193.225.251.147",
    "vlti148" = "193.225.251.183",
    "vlti149" = "193.225.251.203"
  }
}
