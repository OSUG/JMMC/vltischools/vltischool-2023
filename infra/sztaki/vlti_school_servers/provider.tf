terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
    }
  }
}

provider "openstack" {
  application_credential_id = var.auth_data.credential_id
  application_credential_secret = var.auth_data.credential_secret
  auth_url    = var.auth_data.auth_url
  insecure = true
}
