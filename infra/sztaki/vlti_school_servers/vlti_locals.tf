locals {
  subnet_base = split("/", var.vlti_network.network_subnet_range)[0]
  subnet_base_ip = substr(local.subnet_base, 0, length(local.subnet_base) - length(split(".", local.subnet_base)[3]))

  vlti_servers = [
    for i in range(120, 150): 
    {
      name         = "vlti${i}"
      flavor_name  =  var.vlti_servers_config.flavor_name
      image_name   =  var.vlti_servers_config.image_name
      key_pair     =  var.vlti_servers_config.key_pair
      volume_size  =  var.vlti_servers_config.volume_size
      ip_address   = "${local.subnet_base_ip}${i}"
      security_groups = var.vlti_servers_config.security_groups
      user_data       =  file(var.vlti_servers_config.user_data_file)
    }
  ]

  block_devices = [
    {
      source_type           = var.vlti_servers_config.source_type
      destination_type      = var.vlti_servers_config.destination_type
      volume_type           = var.vlti_servers_config.volume_type
      volume_size           = var.vlti_servers_config.volume_size
      boot_index            = 0
      delete_on_termination = true
      uuid                  = var.vlti_servers_config.image_id
    }
  ] 
}
