locals {
  subnet_base = split("/", var.gfs_network.network_subnet_range)[0]
  subnet_base_ip = substr(local.subnet_base, 0, length(local.subnet_base) - length(split(".", local.subnet_base)[3]))

  gfs_servers = [
    for i in range(201, 207): 
    {
      name         = "gfs${i}"
      flavor_name  =  var.gfs_servers_config.flavor_name
      image_name   =  var.gfs_servers_config.image_name
      key_pair     =  var.gfs_servers_config.key_pair
      volume_size  =  var.gfs_servers_config.volume_size
      ip_address   = "${local.subnet_base_ip}${i}"
      security_groups = var.gfs_servers_config.security_groups
      user_data       =  file(var.gfs_servers_config.user_data_file)
    }
  ]

  block_devices = [
    {
      source_type           = var.gfs_servers_config.source_type
      destination_type      = var.gfs_servers_config.destination_type
      volume_type           = var.gfs_servers_config.volume_type
      volume_size           = var.gfs_servers_config.volume_size
      boot_index            = 0
      delete_on_termination = true
      uuid                  = var.gfs_servers_config.image_id
    },
    {
      source_type           = var.gfs_servers_config.raid_source_type
      destination_type      = var.gfs_servers_config.destination_type
      volume_type           = var.gfs_servers_config.volume_type
      volume_size           = var.gfs_servers_config.raid_volume_size
      boot_index            = 1
      delete_on_termination = true
    },
    {
      source_type           = var.gfs_servers_config.raid_source_type
      destination_type      = var.gfs_servers_config.destination_type
      volume_type           = var.gfs_servers_config.volume_type
      volume_size           = var.gfs_servers_config.raid_volume_size
      boot_index            = 2
      delete_on_termination = true
    }
  ] 
}
