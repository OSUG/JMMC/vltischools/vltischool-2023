# Define variables
variable "gfs_network" {
  type = object({
    name = string
    network_id = string
    network_subnet_range = string
    network_subnet_id = string
  })
  default=({
    name = "vlti-school-net"
    network_id = "01aeef63-8d6e-4623-8bae-6b6c4ba13431" # openstack --insecure network list | grep  vlti-school | awk '{print $2}'
    network_subnet_range = "10.1.42.0/24" # openstack --insecure subnet show 0903f873-f9e6-472c-85d3-a88cb4cb229b -f value -c cidr
    network_subnet_id = "0903f873-f9e6-472c-85d3-a88cb4cb229b" # openstack --insecure network list | grep vlti | awk '{print $6}'
  })
}

variable "gfs_servers_config" {
  description = "Configuration for the GlusterFS servers."
  type = object({
    flavor_name = string
    key_pair = string
    volume_size = number
    raid_volume_size = number
    volume_type = string
    security_groups = list(string)
    user_data_file = string
    image_name = string
    image_id = string
    source_type = string
    raid_source_type = string
    destination_type = string
  })
  default = {
    flavor_name = "r2.large"
    key_pair = "vlti"
    volume_size = 20
    raid_volume_size = 500
    volume_type ="SSD_volume"
    security_groups = ["default"]
    user_data_file = "./cloud-init/gfs_cloud-init.yaml"
    image_name = "vlti_gfs_2023-06-12"
    image_id = "4b6bf9c6-bec5-44be-8e79-b403e1e6b8a7" # openstack --insecure image list --project vlti-school | grep vlti_gfs_2023-06-12 | awk '{print $2}'
    source_type = "image"
    raid_source_type = "blank"
    destination_type ="volume"
  }
}
